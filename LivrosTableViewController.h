//
//  LivrosTableViewController.h
//  Ariano
//
//  Created by Gustavo Paiva on 27/06/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItunesAPI.h"

@interface LivrosTableViewController : UITableViewController <ItunesDelegate, UISearchBarDelegate>

@end
