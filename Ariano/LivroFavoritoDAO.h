//
//  LivroFavoritoDAO.h
//  Ariano
//
//  Created by Gustavo Paiva on 29/06/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "NLivroFavorito+CoreDataClass.h"
#import "Livro.h"

@interface LivroFavoritoDAO : NSObject

-(id)init;
-(NLivroFavorito *)novo:(Livro *)livro;
-(void)salvar;
-(NSArray *)getLivrosFavoritos;
-(void)apagar:(NSManagedObject *)objeto;

@end
