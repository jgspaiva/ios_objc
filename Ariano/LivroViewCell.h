//
//  LivroViewCell.h
//  Ariano
//
//  Created by Gustavo Paiva on 27/06/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LivroViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *capaImageView;
@property (weak, nonatomic) IBOutlet UILabel *tituloLabel;
@property (weak, nonatomic) IBOutlet UILabel *autorLabel;
@property (weak, nonatomic) IBOutlet UILabel *precoLabel;

@end
