//
//  DetalhesViewController.h
//  Ariano
//
//  Created by Gustavo Paiva on 28/06/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Livro.h"

@interface DetalhesViewController : UIViewController{
    
}

@property Livro * livro;


@end
