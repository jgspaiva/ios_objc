//
//  DetalhesViewController.m
//  Ariano
//
//  Created by Gustavo Paiva on 28/06/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

#import "DetalhesViewController.h"
#import "LivroFavoritoDAO.h"

@interface DetalhesViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *capaImage;
@property (weak, nonatomic) IBOutlet UILabel *tituloLabel;
@property (weak, nonatomic) IBOutlet UILabel *autorLabel;
@property (weak, nonatomic) IBOutlet UILabel *precoLabel;
@property (weak, nonatomic) IBOutlet UITextView *descricaoTextView;

@end

@implementation DetalhesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.tituloLabel.text = self.livro.titulo;
    self.autorLabel.text = self.livro.autor;
    self.precoLabel.text = self.livro.precoString;
    self.descricaoTextView.text = self.livro.descricao;
    
    [self.capaImage setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:self.livro.capaUrl]]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addFavorito:(id)sender {
    LivroFavoritoDAO * livroDAO = [[LivroFavoritoDAO alloc] init];
    
    [livroDAO novo:self.livro];
    
    [livroDAO salvar];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
