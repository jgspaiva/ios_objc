//
//  ItunesAPI.h
//  Ariano
//
//  Created by Gustavo Paiva on 28/06/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ItunesDelegate <NSObject>

@required
- (void) recebeDados:(NSDictionary *) resposta;

@end

@interface ItunesAPI : NSObject

@property id<ItunesDelegate> origem;

- (void)buscaLivro:(NSString *)termoDaBusca;

@end
