//
//  Livro.h
//  Ariano
//
//  Created by Gustavo Paiva on 28/06/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Livro : NSObject

@property(retain, nonatomic) NSString * autor;
@property(retain, nonatomic) NSString * titulo;
@property(retain, nonatomic) NSString * precoString;
@property(retain, nonatomic) NSString * capaUrl;
@property(retain, nonatomic) NSString * descricao;
@property double preco;

@end
