//
//  LivroViewCell.m
//  Ariano
//
//  Created by Gustavo Paiva on 27/06/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

#import "LivroViewCell.h"

@implementation LivroViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
