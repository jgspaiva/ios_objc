//
//  LivroFavoritoDAO.m
//  Ariano
//
//  Created by Gustavo Paiva on 29/06/17.
//  Copyright © 2017 Gustavo Paiva. All rights reserved.
//

#import "LivroFavoritoDAO.h"

@implementation LivroFavoritoDAO{
    NSManagedObjectContext *context;
}

-(id)init{
    self = [super init];
    
    context = [[((AppDelegate *)[[UIApplication sharedApplication] delegate]) persistentContainer] viewContext];
    
    return self;
}

-(NLivroFavorito *)novo:(Livro *)livro{
    NLivroFavorito * livroFavorito = [NSEntityDescription insertNewObjectForEntityForName:@"NLivroFavorito" inManagedObjectContext:context];
    
    livroFavorito.autor = livro.autor;
    livroFavorito.descricao = livro.autor;
    livroFavorito.precoString = livro.precoString;
    livroFavorito.titulo = livro.titulo;
    livroFavorito.capaUrl = livro.capaUrl;
    livroFavorito.preco = livro.preco;
    
    return livroFavorito;
}

-(void)salvar{
    [context save:nil];
}

-(NSArray *)getLivrosFavoritos{
    NSFetchRequest * request = [NSFetchRequest new];
    
    request.entity = [NSEntityDescription entityForName:@"NLivroFavorito" inManagedObjectContext:context];
    
    NSArray * saida = [context executeFetchRequest:request error:nil];
    
    NSLog(@"%lu linhas na tabela", (unsigned long)[saida count]);
    
    return saida;
}

-(void)apagar:(NSManagedObject *)objeto{
    [context deleteObject:objeto];
    
    [context save:nil];
}
@end
